import 'package:electric_bike_rental_demo/constants.dart';
import 'package:electric_bike_rental_demo/screens/scooter_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CustomScooterCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => ScooterScreen(),
          ),
        );
      },
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        borderOnForeground: true,
        clipBehavior: Clip.antiAlias,
        color: greyColor,
        elevation: 5,
        child: Container(
          padding: EdgeInsets.all(10),
          child: Row(
            children: [
              SvgPicture.asset(
                'assets/bluescooter.svg',
                height: 70,
                width: 70,
              ),
              SizedBox(
                width: 10,
              ),
              Flexible(
                child: Container(
                  child: Text(
                    'This is a title',
                    style: TextStyle(
                      color: mainColor,
                      fontSize: 20,
                    ),
                    // overflow: TextOverflow.ellipsis,
                    softWrap: true,
                    // maxLines: 1,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
