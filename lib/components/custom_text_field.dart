import 'package:electric_bike_rental_demo/constants.dart';
import 'package:flutter/material.dart';

class CustomTextField extends StatelessWidget {
  CustomTextField({required this.text});
  final String text;

  @override
  Widget build(BuildContext context) {
    return TextField(
      keyboardType: TextInputType.emailAddress,
      textAlign: TextAlign.center,
      onChanged: (value) {},
      decoration: kTextFieldDecoration.copyWith(
        hintText: text,
      ),
    );
  }
}
