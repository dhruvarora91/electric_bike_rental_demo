import 'package:electric_bike_rental_demo/components/custom_text_field.dart';
import 'package:electric_bike_rental_demo/constants.dart';
import 'package:electric_bike_rental_demo/screens/home_screen.dart';
import 'package:electric_bike_rental_demo/screens/register_screen.dart';
import 'package:flutter/material.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Scaffold(
          body: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                welcomeText(),
                textFields(context),
                loginButton(context),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget loginButton(BuildContext context) => Padding(
        padding: EdgeInsets.all(16.0),
        child: Material(
          elevation: 5.0,
          color: mainColor,
          borderRadius: BorderRadius.circular(15.0),
          child: MaterialButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => HomeScreen(),
                ),
              );
            },
            // minWidth: 200.0,
            height: 42.0,
            child: Text(
              'Sign In',
              style: TextStyle(
                color: Colors.white,
                fontSize: 20,
              ),
            ),
          ),
        ),
      );

  Container textFields(BuildContext context) => Container(
        padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 20.0),
        child: Column(
          children: <Widget>[
            CustomTextField(text: 'Enter your email'),
            SizedBox(
              height: 15.0,
            ),
            CustomTextField(text: 'Enter your password'),
            SizedBox(height: 7),
            GestureDetector(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => RegisterScreen()));
              },
              child: Text(
                'New User? Register Here!',
                style: TextStyle(
                  color: mainColor,
                  fontSize: 15,
                ),
              ),
            )
          ],
        ),
      );

  Widget welcomeText() => Padding(
        padding: const EdgeInsets.only(
          top: 40.0,
          bottom: 20.0,
          left: 20.0,
          right: 20.0,
        ),
        child: Text(
          'Let\'s sign you in.',
          style: TextStyle(
            color: mainColor,
            fontWeight: FontWeight.bold,
            fontSize: 35,
          ),
        ),
      );
}
