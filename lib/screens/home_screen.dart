import 'package:electric_bike_rental_demo/components/custom_scooter_card.dart';
import 'package:electric_bike_rental_demo/constants.dart';
import 'package:electric_bike_rental_demo/screens/login_screen.dart';
import 'package:electric_bike_rental_demo/screens/register_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            'Rent Your Electric Bike',
            style: TextStyle(
                // color: Color(0xFF3EDBF0,
                ),
          ),
          backgroundColor: mainColor,
        ),
        drawer: Drawer(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: DrawerHeader(
                    child: SvgPicture.asset(
                  'assets/bluescooter.svg',
                )),
              ),
              Expanded(
                flex: 4,
                child: ListView(
                  children: [
                    ListTile(
                      title: Text(
                        'Login',
                        style: TextStyle(fontSize: 18),
                      ),
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => LoginScreen(),
                          ),
                        );
                      },
                    ),
                    ListTile(
                      title: Text(
                        'Register',
                        style: TextStyle(fontSize: 18),
                      ),
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => RegisterScreen(),
                          ),
                        );
                      },
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
        body: ListView(
          children: [
            CustomScooterCard(),
            CustomScooterCard(),
            CustomScooterCard(),
            CustomScooterCard(),
            CustomScooterCard(),
          ],
        ),
      ),
    );
  }
}
