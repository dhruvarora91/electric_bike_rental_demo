import 'package:electric_bike_rental_demo/components/custom_text_field.dart';
import 'package:electric_bike_rental_demo/constants.dart';
import 'package:flutter/material.dart';

class RegisterScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Scaffold(
          body: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Flexible(child: welcomeText()),
                textFields(context),
                registerButton(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget registerButton() => Padding(
        padding: EdgeInsets.all(16.0),
        child: Material(
          elevation: 5.0,
          color: mainColor,
          borderRadius: BorderRadius.circular(15.0),
          child: MaterialButton(
            onPressed: () {},
            // minWidth: 200.0,
            height: 42.0,
            child: Text(
              'Sign Up',
              style: TextStyle(
                color: Colors.white,
                fontSize: 20,
              ),
            ),
          ),
        ),
      );

  Container textFields(BuildContext context) => Container(
        padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 20.0),
        child: Column(
          children: <Widget>[
            CustomTextField(text: 'Name'),
            SizedBox(height: 15.0),
            CustomTextField(text: 'Phone Number'),
            SizedBox(height: 15.0),
            CustomTextField(text: 'Email'),
            SizedBox(height: 15.0),
            CustomTextField(text: 'Password'),
            SizedBox(height: 15.0),
            CustomTextField(text: 'Confirm Password'),
            SizedBox(height: 7),
            GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: Text(
                'Have an Account? Login Here!',
                style: TextStyle(
                  color: mainColor,
                  fontSize: 15,
                ),
              ),
            )
          ],
        ),
      );

  Widget welcomeText() => Padding(
        padding: const EdgeInsets.only(
          top: 40.0,
          bottom: 20.0,
          left: 20.0,
          right: 20.0,
        ),
        child: Text(
          'Let\'s Register Here.',
          style: TextStyle(
            color: mainColor,
            fontWeight: FontWeight.bold,
            fontSize: 35,
          ),
        ),
      );
}
